#!/usr/bin/env bash

export $(cat .env | grep -v ^# | xargs)
echo "Build ${BUILD_NAMESPACE}/${BUILD_PROJECT_NAME}:${BUILD_TAG}"
read -r -p "Continue? [y/N] " response
case "$response" in
    [yY][eE][sS]|[yY])
        export DOCKER_TMPDIR=$(pwd) && docker build -t ${BUILD_NAMESPACE}/${BUILD_PROJECT_NAME}:${BUILD_TAG} --force-rm .
        ;;
    *)
        echo "Exit without building."
        ;;
esac