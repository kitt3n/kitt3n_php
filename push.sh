#!/usr/bin/env bash

export $(cat .env | grep -v ^# | xargs)
echo "Push ${BUILD_NAMESPACE}/${BUILD_PROJECT_NAME}:${BUILD_TAG}"
read -r -p "Continue? [y/N] " response
case "$response" in
    [yY][eE][sS]|[yY])
        docker push ${BUILD_NAMESPACE}/${BUILD_PROJECT_NAME}:${BUILD_TAG}
        ;;
    *)
        echo "Exit without pushing."
        ;;
esac