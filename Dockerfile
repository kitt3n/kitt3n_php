#+++++++++++++++++++++++++++++++++++++++++
# Dockerfile for kitt3n/kitt3n_php:8.0
#+++++++++++++++++++++++++++++++++++++++++

FROM php:8.0-fpm-buster

# enable mods
# RUN a2enmod rewrite \
# 	&& a2enmod headers

COPY etc/             /opt/docker/etc/

# apt
RUN apt-get update \
    && apt-get install -y xz-utils \
    && apt-get install -y libxrender1 \
    && apt-get install -y \
        apt-transport-https \
        lsb-release \
        libbz2-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
        libpq-dev \
        libxml2-dev \
        libxslt1.1 libxslt1-dev \
        libc-client-dev libkrb5-dev \
        ca-certificates \
        wget \
        nano \
        default-mysql-client \
        zsh \
        git \
        gdebi \
        build-essential libssl-dev libxrender-dev \
        gnupg2 \
        sudo \
        vim \
        imagemagick \
        libicu-dev \
        zip \
        libzip-dev \
        poppler-utils \
        ffmpeg \
        html2text \
        pngcrush \
        jpegoptim \
        facedetect \
        graphviz \
        zopfli \
        webp \
        libimage-exiftool-perl \
        cron

RUN mkdir -p /usr/share/man/man1 \
    && apt-get update -y \
    && apt-get install -y openjdk-11-jdk default-jre-headless \
        libreoffice libreoffice-script-provider-python libreoffice-math xfonts-75dpi inkscape libxrender1 libfontconfig1 ghostscript

# pecl
# https://stackoverflow.com/questions/47671108/docker-php-ext-install-mcrypt-missing-folder
RUN pecl install apcu \
	&& pecl install mcrypt-1.0.5 \
	&& docker-php-ext-enable apcu mcrypt

# docker-php-ext-install
RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
	&& docker-php-ext-install imap \
	&& docker-php-ext-configure intl \
	&& docker-php-ext-install intl \
	&& docker-php-ext-install opcache \
	&& docker-php-ext-install bcmath \
	&& docker-php-ext-install calendar \
	&& docker-php-ext-install bz2 \
	&& docker-php-ext-install ctype \
	&& docker-php-ext-install dba \
	&& docker-php-ext-install exif \
	&& docker-php-ext-configure gd --with-freetype --with-jpeg \
	&& docker-php-ext-install gd \
	&& docker-php-ext-install gettext \
	&& docker-php-ext-install mysqli \
	&& docker-php-ext-install pdo_mysql \
	&& docker-php-ext-install pgsql \
	&& docker-php-ext-install soap \
	&& docker-php-ext-install xsl \
	&& docker-php-ext-install zip \
	&& docker-php-ext-install iconv \
	&& docker-php-ext-install phar \
	&& docker-php-ext-enable opcache bcmath calendar bz2 ctype dba gd gettext exif imap mysqli pdo_mysql pgsql soap xsl zip

# ioncube loader
#RUN curl -o ioncube.tar.gz http://downloads3.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz \
#    && tar -xvvzf ioncube.tar.gz \
#    && mv ioncube/ioncube_loader_lin_7.3.so `php-config --extension-dir` \
#    && rm -Rf ioncube.tar.gz ioncube \
#    && docker-php-ext-enable ioncube_loader_lin_7.3

# # wkhtml
#RUN wget https://bitbucket.org/wkhtmltopdf/wkhtmltopdf/downloads/wkhtmltox-0.13.0-alpha-7b36694_linux-jessie-amd64.deb \
RUN gdebi --n /opt/docker/etc/wkhtmltopdf_0.12.5-1_amd64.deb

RUN docker-php-ext-install \
    sockets

#COPY ./zz-socket.conf /usr/local/etc/php-fpm.d/zz-socket.conf

RUN wget https://github.com/imagemin/zopflipng-bin/tree/main/vendor/linux/zopflipng -O /usr/local/bin/zopflipng \
    && chmod 0755 /usr/local/bin/zopflipng

RUN wget https://github.com/imagemin/pngout-bin/tree/main/vendor/linux/x64/pngout -O /usr/local/bin/pngout \
    && chmod 0755 /usr/local/bin/pngout

RUN wget https://github.com/imagemin/advpng-bin/tree/main/vendor/linux/advpng -O /usr/local/bin/advpng \
    && chmod 0755 /usr/local/bin/advpng

RUN wget https://github.com/imagemin/mozjpeg-bin/tree/main/vendor/linux/cjpeg -O /usr/local/bin/cjpeg \
    && chmod 0755 /usr/local/bin/cjpeg

RUN ln -sf /opt/docker/etc/php/development.ini /usr/local/etc/php/conf.d/php.ini

WORKDIR /app

# imagick
RUN apt-get update && apt-get install -y libmagickwand-dev \
   && pecl install imagick-3.7.0 \
   && docker-php-ext-enable imagick

# install imagick
# use github version for now until release from https://pecl.php.net/get/imagick is ready for PHP 8
# RUN git clone https://github.com/Imagick/imagick \
#     && cd imagick \
#     && git checkout 6bcda7c5665799a6dcc39f0d29d7d0b8c19d8c56 \
#     && phpize && ./configure \
#     && make \
#     && make install \
#     && cd ../ \
#     && rm -rf imagick \
#     && docker-php-ext-enable imagick

# composer
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_MEMORY_LIMIT -1
COPY --from=composer:1 /usr/bin/composer /usr/bin/composer1
COPY --from=composer:2 /usr/bin/composer /usr/bin/composer2
RUN ln -s /usr/bin/composer2 /usr/bin/composer

#RUN set -eux; \
#    composer global require "hirak/prestissimo:^0.3" --prefer-dist --no-progress --no-suggest --classmap-authoritative; \
#    composer clear-cache
ENV PATH="${PATH}:/root/.composer/vendor/bin"

# SETUP PHP-FPM CONFIG SETTINGS (max_children / max_requests)
RUN echo 'pm.max_children = 40' >> /usr/local/etc/php-fpm.d/zz-docker.conf && \
    echo 'pm.start_servers = 15' >> /usr/local/etc/php-fpm.d/zz-docker.conf && \
    echo 'pm.min_spare_servers = 15' >> /usr/local/etc/php-fpm.d/zz-docker.conf && \
    echo 'pm.max_spare_servers = 25' >> /usr/local/etc/php-fpm.d/zz-docker.conf

# oh-my-zsh
RUN wget https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh && sh install.sh -unattended && ls ~/.oh-my-zsh
RUN chsh -s /bin/zsh && chsh -s /bin/zsh www-data && ls -alh ~